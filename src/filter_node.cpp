#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
//filters
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/crop_box.h>
//to remove planes
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <iostream>

// globals
ros::Publisher pub;
double leafSize = 0.01, distanceThresh = 0.01;
double pxmin = -1.5, pymin = -1.5, pzmin = -1.5;
double pxmax = +1.5, pymax = +1.5, pzmax = +1.5;
bool planeSegBool = false;
int neighbours = 50, stdevs = 1;

void filterCallback(const sensor_msgs::PointCloud2ConstPtr& msg) {
  pcl::PCLPointCloud2* cloud = new pcl::PCLPointCloud2;
  pcl::PCLPointCloud2ConstPtr cloudPtr(cloud);
  pcl::PCLPointCloud2 cloud_filtered;

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudd (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl_conversions::toPCL(*msg, *cloud);
  pcl::fromPCLPointCloud2(*cloudPtr, *cloudd);

  // noise filtering too slow
  // pcl::PointCloud<pcl::PointXYZRGB>::Ptr noise (new pcl::PointCloud<pcl::PointXYZRGB>);
  //
  // // Create the filtering object
  // pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
  // sor.setInputCloud (cloudd);
  // sor.setMeanK(neighbours);
  // sor.setStddevMulThresh(stdevs);
  // sor.filter(*noise);

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr voxelled (new pcl::PointCloud<pcl::PointXYZRGB>);

  // filter point cloud (voxel_grid)
  pcl::VoxelGrid<pcl::PointXYZRGB> vg;
  vg.setInputCloud(cloudd);
  vg.setLeafSize(leafSize, leafSize, leafSize);
  vg.filter(*voxelled);

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cropped (new pcl::PointCloud<pcl::PointXYZRGB>);
  //cropbox
  pcl::CropBox<pcl::PointXYZRGB> cb;
  cb.setInputCloud(voxelled);
  cb.setMin(Eigen::Vector4f(pxmin, pymin, pzmin, 1.0));
  cb.setMax(Eigen::Vector4f(pxmax, pymax, pzmax, 1.0));
  cb.filter(*cropped);

  pcl::PCLPointCloud2 pcloutput;

  // plane segmentation
  if (planeSegBool) {
    // finish this
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr plane_removed (new pcl::PointCloud<pcl::PointXYZRGB>);

    // plane segmentation
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZRGB> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setDistanceThreshold (distanceThresh);

    seg.setInputCloud(cropped);
    seg.segment (*inliers, *coefficients);

    pcl::ExtractIndices<pcl::PointXYZRGB> extract;

    extract.setInputCloud(cropped);
    extract.setIndices(inliers);
    extract.setNegative(true);
    extract.filter(*plane_removed);


    // noise filtering too slow
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr noise (new pcl::PointCloud<pcl::PointXYZRGB>);
    //
    // // Create the filtering object
    pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
    sor.setInputCloud(plane_removed);
    sor.setMeanK(neighbours);
    sor.setStddevMulThresh(stdevs);
    sor.filter(*noise);

    pcl::toPCLPointCloud2(*noise, pcloutput);
  } else {
    pcl::toPCLPointCloud2(*cropped, pcloutput);
  }


  // convert to ROS point cloud
  sensor_msgs::PointCloud2 output;
  pcl_conversions::moveFromPCL(pcloutput, output);

  // Publish the data.
  pub.publish(output);

  return;
}

void settingCallback(const std_msgs::Float64MultiArray msg) {
  pxmin = msg.data.at(0);
  pymin = msg.data.at(1);
  pzmin = msg.data.at(2);
  pxmax = msg.data.at(3);
  pymax = msg.data.at(4);
  pzmax = msg.data.at(5);
  leafSize = msg.data.at(6);
  planeSegBool = (bool)msg.data.at(7);
  distanceThresh = msg.data.at(8);
  neighbours = (int)msg.data.at(9);
  stdevs = (int)msg.data.at(10);

  // std::cout << pxmin << " " << pymin << " " << pzmin << " " <<
  //              pxmax << " " << pymax << " " << pzmax << " " <<
  //              leafSize << " " << planeSegBool << " " << distanceThresh << " " <<
  //              neighbours << " " << stdevs << std::endl;
  return;
}


int main (int argc, char** argv) {
  ros::init (argc, argv, "filter_node");
  ros::NodeHandle nh;

  ros::Subscriber cloudSub = nh.subscribe<sensor_msgs::PointCloud2> ("/kinect2/sd/points", 1, filterCallback);
  ros::Subscriber settingsSub = nh.subscribe<std_msgs::Float64MultiArray> ("/settings/f", 1, settingCallback);

  pub = nh.advertise<sensor_msgs::PointCloud2> ("/filtered_output", 1);

  ros::spin();
}
