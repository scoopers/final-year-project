#ifndef REGS_H
#define REGS_H

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>


void ICPRegisterWithScale(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& source,
            const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& model,
            const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& result);
#endif
