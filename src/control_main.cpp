#include <QApplication>
#include <ros/ros.h>
#include "control_panel.h"

int main(int argc, char **argv)
{
  if( !ros::isInitialized() )
  {
    ros::init( argc, argv, "control_panel", ros::init_options::AnonymousName );
  }

  QApplication app( argc, argv );

  ControlPanel* cp = new ControlPanel();
  cp->show();

  app.exec();

  delete cp;
}
