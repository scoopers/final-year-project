// pcl includes
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/common/pca.h>
#include <pcl/sample_consensus/lmeds.h>
#include <pcl/sample_consensus/prosac.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_registration.h>
#include <pcl/registration/icp.h>


// header include
#include "RegistrationScaled.h"

#include <iostream>

void ICPRegisterWithScale(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& source,
            const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& model,
            const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& result)
{
  // gather eigenvalues using pca algorithm
  pcl::PCA<pcl::PointXYZRGB> pca;
  pca.setInputCloud(source);
  Eigen::Vector3f eValScene = pca.getEigenValues();

  pca.setInputCloud(model);
  Eigen::Vector3f eValModel = pca.getEigenValues();

  // compute initial scale guess
  // this computation is the variance in the axis of the two point clouds
  double s = sqrt(eValScene[0])/sqrt(eValModel[0]);
  std::cout << "Scale: " << s << "\n";
  
  Eigen::Affine3f scale = Eigen::Affine3f::Identity();
  scale.scale(s);

  pcl::transformPointCloud(*model,*model,scale);
  pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;

  icp.setInputSource(model);
  icp.setInputTarget(source);

  icp.align(*result);
  std::cout << "has converged:" << icp.hasConverged() << " score: " <<
  icp.getFitnessScore() << std::endl;
  std::cout << icp.getFinalTransformation() << std::endl;



}
