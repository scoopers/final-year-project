#ifndef CP_H
#define CP_H

#include <QWidget>
#include <QMainWindow>
#include <ros/ros.h>
//#include "ui_control_panel.h"

namespace Ui {
  class ControlPanel;
}

class ControlPanel: public QMainWindow {
Q_OBJECT

public:
  explicit ControlPanel(QWidget* parent = 0);
  virtual ~ControlPanel();

private Q_SLOTS:
  void saveSettingsPressed();
  void loadSettingsPressed();
  void filterTriggered();
  void clusterTriggered();

private:
  Ui::ControlPanel *ui;
  ros::NodeHandle nh;
  ros::Publisher fpub;
  ros::Publisher ppub;
  ros::Publisher cpub;
};

#endif
