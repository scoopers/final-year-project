#include <ros/ros.h>
#include <ros/package.h>
#include <std_msgs/Float64MultiArray.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

// for loading in pcd files
#include <pcl/io/pcd_io.h>

// for icp
#include <pcl/registration/icp.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/icp_nl.h>



// for segmentation
#include <pcl/filters/extract_indices.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/common/transforms.h>

#include "RegistrationScaled.h"


#include <iostream>
#include <string>
#include <vector>
#include <tf/transform_broadcaster.h>

// globals
ros::Publisher pub;
pcl::PointCloud<pcl::PointXYZRGB>::Ptr object (new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::PointCloud<pcl::PointXYZRGB>::Ptr scene (new pcl::PointCloud<pcl::PointXYZRGB>);
bool firstMessage = false;
double clusterTol = 0.02;
// basically how big or small we're letting an object be to count as a trackable object
int minClustSize = 100, maxClustSize = 25000;

void processCallback(const sensor_msgs::PointCloud2ConstPtr& msg) {
  pcl::PCLPointCloud2 input;
  pcl_conversions::toPCL(*msg, input);
  pcl::fromPCLPointCloud2(input, *scene);
  if(firstMessage) {

    //pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
    //pcl::GeneralizedIterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
    pcl::IterativeClosestPointNonLinear<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
    icp.setInputSource(object);
    icp.setInputTarget(scene);

    // apply icp
    pcl::PointCloud<pcl::PointXYZRGB> result;
    icp.align(result);

    //compute centroid of object
    Eigen::Vector4f centroid;
    pcl::compute3DCentroid(result, centroid);
    std::cout << "Object Centroid: " << centroid << "\n";

    // get rotation
    Eigen::Affine3f trans(icp.getFinalTransformation());
    float roll, pitch, yaw;
    pcl::getEulerAngles(trans,roll,pitch,yaw);
    std::cout << "Rotation: " << "(" << roll << pitch << yaw << ")\n";

    // broadcast transform frame
    static tf::TransformBroadcaster tb;
    tf::Transform tf;
    tf.setOrigin(tf::Vector3(centroid[0],centroid[1],centroid[2]));
    tf::Quaternion q;
    q.setRPY(roll,pitch,yaw);
    tf.setRotation(q);
    tb.sendTransform(tf::StampedTransform(tf, ros::Time::now(), "/kinect2_ir_optical_frame", "/object"));

    // disabled attempt at ICP scaling algorithm
    // pcl::PointCloud<pcl::PointXYZRGB>::Ptr transformedObj;
    // transformedObj.reset(new pcl::PointCloud<pcl::PointXYZRGB>);

    // ICPRegisterWithScale(scene, object, transformedObj);

    // convert back to ROS format
    pcl::PCLPointCloud2 pcloutput;
    pcl::toPCLPointCloud2(result, pcloutput);

    // convert to ROS point cloud
    sensor_msgs::PointCloud2 output;
    pcl_conversions::moveFromPCL(pcloutput, output);
    output.header.frame_id = "/kinect2_ir_optical_frame";

    // Publish the data.
    // if(icp.hasConverged())
    pub.publish(output);
  }
  return;
}


void settingCallback(const std_msgs::Float64MultiArray msg) {
  return;
}

void clusterCallback(const std_msgs::Float64MultiArray msg) {
  object->points.clear();
  clusterTol = msg.data.at(0);
  minClustSize = (int)msg.data.at(1);
  maxClustSize = (int)msg.data.at(2);

  // do clustering/segmentation using Euclidean Clustering
  pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
  tree->setInputCloud(scene);
  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> ec;
  ec.setClusterTolerance(clusterTol); // tolerance
  ec.setMinClusterSize(minClustSize); // minclustersize
  ec.setMaxClusterSize(maxClustSize); // maxclustersize
  ec.setSearchMethod(tree);
  ec.setInputCloud(scene);
  ec.extract(cluster_indices);
  std::cout << cluster_indices.size() << std::endl;;
  for (std::vector<int>::const_iterator pit = cluster_indices.at(0).indices.begin(); pit != cluster_indices.at(0).indices.end(); ++pit)
    object->points.push_back(scene->points[*pit]);
  firstMessage = true;
  return;
}

int main (int argc, char** argv) {
  ros::init (argc, argv, "process_node");
  ros::NodeHandle nh;

  ros::Subscriber cloudSub = nh.subscribe<sensor_msgs::PointCloud2> ("/filtered_output", 1, processCallback);
  ros::Subscriber settingsSub = nh.subscribe<std_msgs::Float64MultiArray> ("/settings/p", 1, settingCallback);
  ros::Subscriber clusterSub = nh.subscribe<std_msgs::Float64MultiArray> ("/settings/c", 1, clusterCallback);

  pub = nh.advertise<sensor_msgs::PointCloud2> ("/icp_output", 1);

  ros::spin();
}
