#include <QSlider>
#include <QLabel>
#include <QMainWindow>
#include <QSpinBox>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QIODevice>
#include <QString>
#include <QTextStream>
#include <QDataStream>
#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>

#include "control_panel.h"
#include "ui_control_panel.h"

// control panel constructor
ControlPanel::ControlPanel(QWidget* parent) :
  QMainWindow(parent),
  ui(new Ui::ControlPanel) {
  ui->setupUi(this);
  this->setWindowTitle("Control Panel");

  fpub = nh.advertise<std_msgs::Float64MultiArray> ("/settings/f", 1);
  ppub = nh.advertise<std_msgs::Float64MultiArray> ("/settings/p", 1);
  cpub = nh.advertise<std_msgs::Float64MultiArray> ("/settings/c", 1);

  // connections for filter parameters
  connect(ui->save_settings, SIGNAL(triggered()), this, SLOT(saveSettingsPressed()));
  connect(ui->load_settings, SIGNAL(triggered()), this, SLOT(loadSettingsPressed()));
  connect(ui->cropxmin_spin, SIGNAL(editingFinished()), this, SLOT(filterTriggered()));
  connect(ui->cropymin_spin, SIGNAL(editingFinished()), this, SLOT(filterTriggered()));
  connect(ui->cropzmin_spin, SIGNAL(editingFinished()), this, SLOT(filterTriggered()));
  connect(ui->cropxmax_spin, SIGNAL(editingFinished()), this, SLOT(filterTriggered()));
  connect(ui->cropymax_spin, SIGNAL(editingFinished()), this, SLOT(filterTriggered()));
  connect(ui->cropzmax_spin, SIGNAL(editingFinished()), this, SLOT(filterTriggered()));
  connect(ui->voxelgrid_slider, SIGNAL(sliderReleased()), this, SLOT(filterTriggered()));
  connect(ui->planeseg_check, SIGNAL(released()), this, SLOT(filterTriggered()));
  connect(ui->neighbours_spin, SIGNAL(editingFinished()), this, SLOT(filterTriggered()));
  connect(ui->stdevs_spin, SIGNAL(editingFinished()), this, SLOT(filterTriggered()));
  connect(ui->distancethresh_spin, SIGNAL(editingFinished()), this, SLOT(filterTriggered()));

  connect(ui->seg_button, SIGNAL(released()), this, SLOT(clusterTriggered()));
  // connections for segmentation parameters
}

void ControlPanel::filterTriggered() {
  std_msgs::Float64MultiArray settingValues;
  settingValues.data.push_back(ui->cropxmin_spin->value());
  settingValues.data.push_back(ui->cropymin_spin->value());
  settingValues.data.push_back(ui->cropzmin_spin->value());
  settingValues.data.push_back(ui->cropxmax_spin->value());
  settingValues.data.push_back(ui->cropymax_spin->value());
  settingValues.data.push_back(ui->cropzmax_spin->value());
  settingValues.data.push_back((double)ui->voxelgrid_slider->value()/100);
  if(ui->planeseg_check->checkState()) {
    settingValues.data.push_back(1.0);
  } else {
    settingValues.data.push_back(0.0);
  }
  settingValues.data.push_back(ui->distancethresh_spin->value());
  settingValues.data.push_back(ui->neighbours_spin->value());
  settingValues.data.push_back(ui->stdevs_spin->value());
  fpub.publish(settingValues);
  return;
}

void ControlPanel::clusterTriggered() {
  std_msgs::Float64MultiArray settingValues;
  settingValues.data.push_back(ui->clustertol_spin->value());
  settingValues.data.push_back((double)ui->mincluster_spin->value());
  settingValues.data.push_back((double)ui->maxcluster_spin->value());
  cpub.publish(settingValues);
  return;
}

// for saving parameter settings
void ControlPanel::saveSettingsPressed() {
  QString fileName = QFileDialog::getSaveFileName(this,
                                                  tr("Save parameter settings"), "",
                                                  tr("6D Pose Project Settings (*.sdps);;All Files (*)"));
  if(fileName.isEmpty()) {
    return;
  } else {
    QFile file(fileName + ".sdps");
    if(!file.open(QIODevice::WriteOnly)) {
      QMessageBox::information(this, tr("Cannot open file."), file.errorString());
      return;
    }

    QDataStream out(&file);
    out << ui->cropxmin_spin->value() <<
           ui->cropymin_spin->value() <<
           ui->cropzmin_spin->value() <<
           ui->cropxmax_spin->value() <<
           ui->cropymax_spin->value() <<
           ui->cropzmax_spin->value() <<
           (double)ui->voxelgrid_slider->value();
    file.close();
  }
  return;
}

// for loading in parameter settings
void ControlPanel::loadSettingsPressed() {
  QString fileName = QFileDialog::getOpenFileName(this,
                                                  tr("Load parameter settings"), "",
                                                  tr("6D Pose Project Settings (*.sdps);;All Files (*)"));

  if(fileName.isEmpty()) {
    return;
  } else {
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
      QMessageBox::information(this, tr("Cannot open file."), file.errorString());
      return;
    }

    static double array[7];
    QDataStream in(&file);
    in >> array[0] >> array[1] >> array[2] >> array[3] >> array[4] >> array[5] >> array[6];
    ui->cropxmin_spin->setValue(array[0]);
    ui->cropymin_spin->setValue(array[1]);
    ui->cropzmin_spin->setValue(array[2]);
    ui->cropxmax_spin->setValue(array[3]);
    ui->cropymax_spin->setValue(array[4]);
    ui->cropzmax_spin->setValue(array[5]);
    ui->voxelgrid_slider->setValue((int)array[6]);
    file.close();
  }
  return;
}

// Destructor.
ControlPanel::~ControlPanel() {
  delete ui;
}
